// ............................................................................
// Imports
// ............................................................................
const AWS = require('aws-sdk');
const dbServer = require('./db');
const ObjectId = require('mongodb').ObjectId;
// ............................................................................
// Constants
// ............................................................................
const { MONGO_DB_NAME, MONGO_POSTS_COLLECTION_NAME } = process.env;
const DEFAULT_ERROR = 'Error';
// ............................................................................
// Configuration
// ............................................................................
AWS.config.update({ region: 'us-east-2' });
// ............................................................................
// Entrypoint
// ............................................................................
exports.handler = (event, context, callback) => {
  console.log(event);
  // The following line is critical for performance reasons to allow re-use of database
  // connections across calls to this Lambda function and avoid closing the database
  // connection.The first call to this lambda function takes about 5 seconds to complete,
  // while subsequent, close calls will only take a few hundred milliseconds.
  context.callbackWaitsForEmptyEventLoop = false;

  if (event.httpMethod === 'OPTIONS') {
  console.log('@@@ SENDING OPTIONS RESPONSE @@@');
  callback(null, {
    "isBase64Encoded": false,
    "statusCode": 200,
    "headers": {
    'Access-Control-Allow-Methods': '*',
    'Access-Control-Allow-Headers': '*',
    'Access-Control-Allow-Origin': '*'
    },
    // "multiValueHeaders": { "headerName": ["headerValue", "headerValue2", ...], ... },
    "body": ''
  });
  return;
  }

  processEvent(event, context, (error, result) => {
  if (error) {
    callback(error);
  }
  else {
    callback(null, {
    "isBase64Encoded": false,
    "statusCode": 200,
    "headers": { 
      'Access-Control-Allow-Methods': '*',
      'Access-Control-Allow-Headers': '*',
      'Access-Control-Allow-Origin': '*'
    },
    // "multiValueHeaders": { "headerName": ["headerValue", "headerValue2", ...], ... },
    "body": JSON.stringify(result)
    });
  }
  });
};
// ............................................................................
// Main
// ............................................................................
function processEvent(event, context, callback) {
  switch (event.httpMethod) {
  case 'GET':
    onGetRequest(...arguments);
    break;
  case 'POST':
    onPostRequest(...arguments);
    break;
  case 'PUT':
    onPutRequest(...arguments);
    break;
  default:
    callback("Method not supported");
    break;
  }
}

function onGetRequest(event, context, callback) {
  return dbServer.connect(MONGO_DB_NAME)
  .then(function (db) {
    console.log('Received DB');
    const { channel, postId = null } = event.queryStringParameters; // TODO: Unsafe operation needs to be sanitized.
    const posts = db.collection(MONGO_POSTS_COLLECTION_NAME);
    const query = postId !== null ? { _id: new ObjectId(postId) } : { channel };
    
    console.log('Retrieved Posts Collection: ', posts);
    
    posts.find(query).toArray((error, results) => { 
      console.log('Queried Posts: Error: ', error, ' Results: ', results);
      if (error) {
        throw error;
      }
      else callback(null, postId ? results[0] : results);
    });
  })
  .catch(error => {
    console.error('DiscourseFeed.onGetRequest(): an error occurred:- ', error);
    callback(DEFAULT_ERROR);
  });
}

function onPostRequest(event, context, callback) {
  return dbServer.connect(MONGO_DB_NAME)
  .then(function(db) {
    console.error('DiscourseFeed.onPostRequest(): received data:- ', event.body);
    const posts = db.collection(MONGO_POSTS_COLLECTION_NAME);
    const post = event.body;

    post.createdDate = new Date(),

    posts.insertOne(post, function(err, response) {
      if (err) {
        console.error('DiscourseFeed.onPostRequest(): insertion error:- ', err);
        callback(DEFAULT_ERROR);
      }
      else {      
        console.error('DiscourseFeed.onPostRequest(): insert successful. Data:- ', response.result);
        callback(null, response.insertedCount);
      }
    })
  })
  .catch(error => {
    console.error('DiscourseFeed.onPostRequest(): an error occurred:- ', error);
    callback(DEFAULT_ERROR);
  });
}

function onPutRequest(event, context, callback) {
  return dbServer.connect(MONGO_DB_NAME)
    .then(function(db) {
      const posts = db.collection(MONGO_POSTS_COLLECTION_NAME);
      const { comment, postId } = event.body;

      posts.update({ _id: new ObjectId(postId) }, { $addToSet: { comments: comment } },
        function(err, post) {
          if (err) {
            console.error('DiscourseFeed.onPutRequest(): update error: ', err);
            callback(DEFAULT_ERROR);
          }
          else {
            console.log("DiscourseFeed.onPutRequest(): update comments successful!");
            callback(null, post);
          }
        });
    })
    .catch(error => {
      console.error(
        'DiscourseFeed.onPutRequest(): an error occurred:-',
        error);
      callback(DEFAULT_ERROR);
    })
}
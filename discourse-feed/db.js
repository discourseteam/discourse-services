// ............................................................................
// Imports
// ............................................................................
const AWS = require('aws-sdk');
const MongoClient = require('mongodb').MongoClient;
// ............................................................................
// Constants
// ............................................................................
const ENV_MONGO_DB_URI = 'MONGO_DB_URI';
const __TEST_ENV_MONGO_DB_URI = '__TEST@' + ENV_MONGO_DB_URI;
// ............................................................................
// Vars
// ............................................................................
let cachedConnectionUri = null;
let cachedDbClients = {};
// ............................................................................
// Exports
// ............................................................................
module.exports = {
  connect: function(name) {
    return getConnectionString()
      .then(connectionUri => getDatabase(name, connectionUri));
  }
}
// ............................................................................
// Helpers
// ............................................................................
function getConnectionString() {
  return new Promise((resolve, reject) => {
    const env = process.env;

    if (cachedConnectionUri === undefined || cachedConnectionUri === null ) {
      if (__TEST_ENV_MONGO_DB_URI in process.env) {
        resolve(cachedConnectionUri = env[__TEST_ENV_MONGO_DB_URI]);
      }
      else {
        // Decrypt code should run once and variables stored outside of the
        // function handler so that these are decrypted once per container
        const kms = new AWS.KMS();
        const kmsOptions = {
          CiphertextBlob: new Buffer(env[ENV_MONGO_DB_URI], 'base64'),
        };

        kms.decrypt(kmsOptions, (error, data) => {
          if (error) {
            reject(error);
          }
          // @ts-ignore
          else resolve(cachedConnectionUri = data.Plaintext.toString('ascii'));
        });
      }
    }
    else {
      resolve(cachedConnectionUri);
    }
  });
}

function getDatabase(name, connectionUri) {
  return new Promise((resolve, reject) => {
    const cachedClient = cachedDbClients[name];

    if (cachedClient && cachedClient.db(name).serverConfig.isConnected()) {
      console.log('Mongo: reusing cached connection!');
      resolve(cachedClient.db(name));
    }
    else {
      console.log('Mongo: establishing new connection...');
      try {
        MongoClient.connect(connectionUri, { useNewUrlParser: true },
          (err, client) => err ? reject(err) : resolve((cachedDbClients[name] = client).db(name)));
      }
      catch (error) {
        reject(error);
      }
    }
  });
}
